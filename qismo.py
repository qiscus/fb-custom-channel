import requests
import os

QISMO_BASE_URL = os.getenv("QISMO_BASE_URL")
QISMO_APP_ID = os.getenv("QISMO_APP_ID")
QISMO_AUTH_TOKEN = os.getenv("QISMO_AUTH_TOKEN")
CHANNEL_IDENTIFIER_KEY = os.getenv("CHANNEL_IDENTIFIER_KEY")
APP_DOMAIN_URL = os.getenv("APP_DOMAIN_URL")
BADGE_URL = os.getenv("BADGE_URL")


class Qismo:
    def send_message(user_id, fullname, message):
        data = {}
        data["identifier_key"] = CHANNEL_IDENTIFIER_KEY
        data["user_id"] = f"{user_id}_customer_{QISMO_APP_ID}@qismo.com"
        data["name"] = fullname
        data["message"] = message

        url = f"{QISMO_BASE_URL}/{QISMO_APP_ID}/custom_channel"
        res = requests.post(url, data=data)

        if res.status_code is not 200:
            raise Exception("Sending message to Qismo Failed")
        else:
            json_res = res.json()
            data = json_res["data"]

            return data

    def send_attachment_message(user_id, fullname, attachment_url):
        data = {}
        data["identifier_key"] = CHANNEL_IDENTIFIER_KEY
        data["user_id"] = f"{user_id}_customer_{QISMO_APP_ID}@qismo.com"
        data["name"] = fullname
        data["message"] = f"[file]{attachment_url}[/file]"

        url = f"{QISMO_BASE_URL}/{QISMO_APP_ID}/custom_channel"
        res = requests.post(url, data=data)

        if res.status_code is not 200:
            raise Exception("Sending message to Qismo Failed")
        else:
            json_res = res.json()
            data = json_res["data"]

            return data

    def get_custom_channel():
        callback = {}

        url = f"{QISMO_BASE_URL}/api/v1/channels"

        headers = {}
        headers["Authorization"] = str(QISMO_AUTH_TOKEN)

        res = requests.get(url, headers=headers)

        if res.status_code is not 200:
            raise Exception("Failed to get list channels")

        json_response = res.json()
        data = json_response["data"]
        custom_channels = data["custom_channels"]

        for channel in custom_channels:
            if channel["identifier_key"] == CHANNEL_IDENTIFIER_KEY:
                callback["is_success"] = True
                callback["channel"] = channel

                return callback

    def update_channel(channel_id, channel_name):
        callback = {}

        url = f"{QISMO_BASE_URL}/api/v1/custom_channel/connect/update"

        headers = {}
        headers["Authorization"] = str(QISMO_AUTH_TOKEN)

        payload = {}
        payload["id"] = channel_id
        payload["name"] = channel_name
        payload["webhook_url"] = f"{APP_DOMAIN_URL}/qismo-message"
        payload["identifier_key"] = CHANNEL_IDENTIFIER_KEY
        payload["logo_url"] = BADGE_URL

        res = requests.post(url, headers=headers, data=payload)
        if res.status_code is not 200:
            raise Exception("Error updating Custom Channel")
        else:
            callback["is_success"] = True
            return callback
